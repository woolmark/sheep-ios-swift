//
//  SheepView.swift
//  sheep-ios-swift
//
//  Created by Naoki Takimura on 6/9/14.
//  Copyright (c) 2014 Naoki Takimura. All rights reserved.
//

import Foundation
import UIKit

/**
 SheepView.
 */
class SheepView : UIView {

    let DEFAULT_HEIGHT: CGFloat = 200
    
    let DEFAULT_WIDTH: CGFloat = 200

    let GROUND_HEIGHT_RATIO: CGFloat = 0.9
    
    let JUMP_FRAME = 3

    let X_SPEED = 5

    let Y_SPEED = 3

    let KEY_X = "x"

    let KEY_Y = "y"
    
    let KEY_JUMP = "jump"
    
    let KEY_JUMP_X = "jumpX"

    /**
     a fence image.
     */
    let fence: UIImage = UIImage(named: "fence")!

    /**
     a sheep image, index number is 0.
     */
    let sheep00: UIImage = UIImage(named: "sheep00")!

    /**
     a sheep image, index number is 1.
     */
    let sheep01: UIImage = UIImage(named: "sheep01")!

    /**
     a ground color, that is light green.
     */
    let groundColor: UIColor = UIColor(red: 0.471, green: 1, blue: 0.471, alpha: 1)

    /**
     a sky color, that is light blue.
     */
    let skyColor: UIColor = UIColor(red: 0.5, green: 0.5, blue: 1, alpha: 1)

    var count:Int = 0

    var scale: CGFloat = 1

    var groundHeight: CGFloat = 0

    var sheepAddFlag: Bool = false

    /**
     animation count to draw a sheep image animation.
     */
    var stretch: Int = 0

    /**
     the sheeps array.
     */
    var sheep: [NSMutableDictionary] = [NSMutableDictionary]()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)

        let w = frame.size.width
        let h = frame.size.height
        if w < h {
            scale = floor(w / DEFAULT_WIDTH)
        } else {
            scale = floor(h / DEFAULT_HEIGHT)
        }
        if scale < 1 {
            scale = 1
        }

        groundHeight = floor(fence.size.height * scale * GROUND_HEIGHT_RATIO)

        // init sheep
        for _ in 0..<100 {
            let aSheep: NSMutableDictionary = NSMutableDictionary()
            removeSheep(aSheep)
            sheep.append(aSheep)
        }

        addSheep(sheep[0])
    }

    /**
     Drawing SheepView scene in the rect.
     @param rect screen rect
     */
    override func draw(_ rect: CGRect) {

        skyColor.set()
        UIRectFill(CGRect(x: 0, y: 0, width: rect.width, height: rect.height))

        groundColor.set()
        UIRectFill(CGRect(x: 0, y: frame.size.height - groundHeight, width: rect.width, height: groundHeight))

        fence.draw(in: CGRect(
            x: (frame.size.width - fence.size.width * scale) / 2,
            y: frame.size.height - fence.size.height * scale,
            width: fence.size.width * scale,
            height: fence.size.height * scale))

        for i in 0..<sheep.count {
            let aSheep: NSMutableDictionary = sheep[i]
            
            if aSheep.object(forKey: KEY_Y) as! Int >= 0 {

                var image: UIImage
                if aSheep.object(forKey: KEY_JUMP) as! Int >= 0 {
                    image = sheep01
                } else {
                    if (stretch == 0) {
                        image = sheep00
                    } else {
                        image = sheep01
                    }
                }

                image.draw(in: CGRect(
                    x: CGFloat(aSheep.object(forKey: KEY_X) as! Int),
                    y: CGFloat(aSheep.object(forKey: KEY_Y) as! Int),
                    width: image.size.width * scale,
                    height: image.size.height * scale))

            }

        }

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        sheepAddFlag = true
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        sheepAddFlag = false
    }

    /**
     Updating SheepView frame and redraw it.
     */
    func update() {

        if sheepAddFlag {
            for i in 0..<sheep.count {
                let aSheep: NSMutableDictionary = sheep[i]
                if (aSheep.object(forKey: KEY_Y) as! Int) < 0 {
                    addSheep(aSheep)
                    break;
                }
            }
        }

        for i in 0..<sheep.count {
            let aSheep: NSMutableDictionary = sheep[i]
            if (aSheep.object(forKey: KEY_Y) as! Int) < 0 {
                continue
            }

            aSheep.setObject(aSheep.object(forKey: KEY_X) as! Int - X_SPEED * Int(scale), forKey: KEY_X as NSCopying)

            if (aSheep.object(forKey: KEY_JUMP) as! Int) < 0
                    && (aSheep.object(forKey: KEY_JUMP_X) as! Int) <= (aSheep.object(forKey: KEY_X) as! Int)
                    && (aSheep.object(forKey: KEY_X) as! Int) <= ((aSheep.object(forKey: KEY_JUMP_X) as! Int) + X_SPEED * Int(scale)){
                aSheep.setObject(0, forKey: KEY_JUMP as NSCopying)
            }

            if aSheep.object(forKey: KEY_JUMP) as! Int >= 0 {
                if (aSheep.object(forKey: KEY_JUMP) as! Int) < JUMP_FRAME {
                    aSheep.setObject(aSheep.object(forKey: KEY_Y) as! Int - Y_SPEED * Int(scale), forKey: KEY_Y as NSCopying)
                } else if (aSheep.object(forKey: KEY_JUMP) as! Int) < JUMP_FRAME * 2 {
                    aSheep.setObject(aSheep.object(forKey: KEY_Y) as! Int + Y_SPEED * Int(scale), forKey: KEY_Y as NSCopying)
                }

                aSheep.setObject(aSheep.object(forKey: KEY_JUMP) as! Int + 1, forKey: KEY_JUMP as NSCopying)
            }

            if aSheep.object(forKey: KEY_JUMP) as! Int == JUMP_FRAME {
                count += 1
            }

            if (aSheep.object(forKey: KEY_X) as! Int) < -1 * Int(sheep00.size.width * scale) {
                if i == 0 {
                    addSheep(aSheep)
                } else {
                    removeSheep(aSheep)
                }
            }
            
        }

        stretch = 1 - stretch

        setNeedsDisplay()
    }

    func addSheep(_ aSheep: NSMutableDictionary) {

        aSheep.setObject(Int(frame.size.width + sheep00.size.width * scale), forKey: KEY_X as NSCopying)
        aSheep.setObject((Int(frame.size.height - groundHeight) + Int(arc4random() % UInt32(groundHeight - sheep00.size.height * scale))) as Int, forKey: KEY_Y as NSCopying)
        aSheep.setObject(Int.min, forKey: KEY_JUMP as NSCopying)
        aSheep.setObject(calcJumpX(aSheep.object(forKey: KEY_Y) as! Int), forKey: KEY_JUMP_X as NSCopying)

    }

    func calcJumpX(_ y: Int!) -> Int {
        // y = -1 * fence.height / fence.width * (x - (width - fence.width) / 2) + height

        let w = Int(frame.size.width)
        let h = Int(frame.size.height)
        let fw = Int(fence.size.width * scale)
        let fh = Int(fence.size.height * scale)

        return -1 * (y - h) * fw / fh + (w - fw) / 2
    }

    func removeSheep(_ aSheep: NSMutableDictionary) {
        
        aSheep.setObject(0, forKey: KEY_X as NSCopying)
        aSheep.setObject(-1, forKey: KEY_Y as NSCopying)
        aSheep.setObject(0, forKey: KEY_JUMP as NSCopying)
        aSheep.setObject(0, forKey: KEY_JUMP_X as NSCopying)

    }

}
