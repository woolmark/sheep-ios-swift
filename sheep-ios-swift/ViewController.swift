//
//  ViewController.swift
//  sheep-ios-swift
//
//  Created by Naoki Takimura on 6/9/14.
//  Copyright (c) 2014 Naoki Takimura. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    @IBOutlet var counterLabel : UILabel?

    @IBOutlet var sheepView : SheepView?

    var timer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        sheepView = (view as! SheepView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func validateTimer() {

        // start timer
        timer = Timer.scheduledTimer(timeInterval: 0.1, target: self,
            selector: #selector(ViewController.update), userInfo: nil, repeats: true)

    }

    func invalidateTimer() {

        timer?.invalidate()

    }

    func update() {
        sheepView?.update()
        if let value = sheepView?.count {
            counterLabel?.text = "\(value) sheep"
        }
    }

}

