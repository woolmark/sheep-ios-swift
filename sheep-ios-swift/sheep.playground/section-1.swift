// Playground - noun: a place where people can play

import Foundation
import UIKit

class SheepView : UIView {

    let fence: UIImage = UIImage(named: "fence")

    let sheep00: UIImage = UIImage(named: "sheep00")

    let sheep01: UIImage = UIImage(named: "sheep01")

    let groundColor: UIColor = UIColor(red: 0.471, green: 1, blue: 0.471, alpha: 1)

    let skyColor: UIColor = UIColor(red: 0.5, green: 0.5, blue: 1, alpha: 1)

    override func drawRect(rect: CGRect) {

        groundColor.set()
        UIRectFill(CGRect(x: 0, y: 0, width: rect.width, height: rect.height))

        skyColor.set()
        UIRectFill(CGRect(x: 0, y: 0, width: rect.width, height: 20))

        fence.drawAtPoint(CGPoint(x: 20, y: 20))

    }
    
    func update() {
        
    }

}

var view = SheepView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))


